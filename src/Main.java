import models.Entry;
import models.Helper;
import models.Profile;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.Buffer;
import java.util.ArrayList;

public class Main {

    private static ArrayList<Entry> entries = new ArrayList<Entry>();
    private static ArrayList<Helper> helpers = new ArrayList<Helper>();
    private static ArrayList<Profile> profiles = new ArrayList<Profile>();

    public static void main(String[] args) {
        boolean exit = false;
        System.out.println("Hinweis: Alle Eingaben müssen mit Enter bestätigt werden.");

        while(!exit) {
            System.out.println("\nWas wollen sie tun? Geben Sie die entsprechende Option ein");
            System.out.println("1 Neuen Dienstbucheintrag anlegen");
            System.out.println("2 Kompetenzbereich anlegen");
            System.out.println("3 Dienstbucheintrag bearbeiten");
            System.out.println("4 Kompetenzbereich bearbeiten");
            System.out.println("5 Alle Einträge ansehen");
            System.out.println("6 Alle Kompetenzbereiche ansehen");
            System.out.println("7 Programm beenden");

            int i = readNumberFromConsole();

            switch (i) {
                case 1 :
                    createNewEntry();
                    break;
                case 2:
                    createNewProfile();
                    break;
                case 3:
                    editEntry();
                    break;
                case 4:
                    editProfile();
                    break;
                case 5:
                    seeAllEntries();
                    break;
                case 6:
                    seeAllProfiles();
                    break;
                case 7:
                    //System.exit(0);
                    exit = true;
                    break;
            }
        }

        //raus aus dem Loop
        System.exit(0);

    }

    private static int readNumberFromConsole() {
        BufferedReader br = new BufferedReader((new InputStreamReader(System.in)));
        String s = null;
        try {
            s = br.readLine();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return Integer.parseInt(s);
    }

    private static void editProfile() {
        seeAllProfiles();
        if (profiles.size() == 0) {
            return;
        }
        System.out.println("Geben Sie bitte die Nummer des Kompetenzbereiches an den Sie bearbeiten wollen");
        int index = readNumberFromConsole();
        System.out.println("Geben Sie bitte einen Namen für den Kompetenzbereich ein.");
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        try {
            profiles.get(index).setProfileName(br.readLine());
        } catch (IOException e) {
            e.printStackTrace();
        }
        System.out.println("Geben Sie bitte die nötige Fähigkeit für den Kompetenzbereich ein.");
        try {
            profiles.get(index).setSkill(br.readLine());
        } catch (IOException e) {
            e.printStackTrace();
        }
        System.out.println("Profil wurde editiert");
    }

    private static void editEntry() {
        //ToDo
    }

    private static void seeAllEntries() {
        if (entries.size() == 0) {
            System.out.println("Es wurde noch keine Einträge getätigt.");
        }
        else {
            System.out.println("Die Einträge sind:");
            int index = 0;
            for (Entry e : entries) {
                System.out.print(index++ + ": ");
                System.out.print(helpers.get(e.getHelperID()).getSurname() + " " + helpers.get(e.getHelperID()).getName());
                System.out.print(" im Bereich " + profiles.get(e.getProfileID()).getProfileName());
                System.out.println(e);
                System.out.println("Er kriegt am Ende des Tages einen Verdienst von: " + e.calculateEarning() + " Euro.");
            }
        }
    }

    private static void seeAllProfiles() {
        if (profiles.size() == 0) {
            System.out.println("Es wurden noch keine Kompetenzbereiche angelegt.");
        }
        else {
            System.out.println("Die Profile sind:");
            int index = 0;
            for (Profile p : profiles) {
                System.out.print(index++ + ": ");
                System.out.println(p);
            }
        }
    }

    private static void createNewProfile() {
        System.out.println("Geben Sie bitte einen Namen für den Kompetenzbereich ein.");
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        String profileName = null;
        String skill = null;
        try {
            profileName = br.readLine();
        } catch (IOException e) {
            e.printStackTrace();
        }
        System.out.println("Geben Sie bitte die nötige Fähigkeit für den Kompetenzbereich ein.");
        try {
            skill = br.readLine();
        } catch (IOException e) {
            e.printStackTrace();
        }

        Profile profile = new Profile(profileName,skill);
        profiles.add(profile);
        System.out.println("Kompetenzbereich wurde erstellt.");
    }

    private static void createNewEntry() {
        if (profiles.size() == 0 ) {
            System.out.println("Bitte legen Sie zuerst Kompetenzbereiche an bevor Sie Einträge erstellen.");
        }
        else {
            //entry erstellen
            BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
            String name = null;
            String surname = null;
            String iban = null;
            String helperSkill = null;
            String startTime =null;
            String endTime = null;
            Double moneyPerHour = null;

            try {
                System.out.println("Geben Sie bitte den Nachnamen ein!");
                name = br.readLine();
                System.out.println("Geben Sie bitte den Vornamen ein!");
                surname = br.readLine();
                System.out.println("Geben Sie bitte die IBAN ein");
                iban = br.readLine();
                System.out.println("Welchen Skill hat der Mitarbeiter?");
                helperSkill = br.readLine();
            } catch (IOException e) {
                e.printStackTrace();
            }

            System.out.println("Welchen Kompetenzbereich soll dem Helfer zugeordnet werden?");
            seeAllProfiles();
            //ToDo check if profile skill == helper skill, loop till true
            int profileID = readNumberFromConsole();
            Helper helper = new Helper(name,surname,iban,helperSkill);
            helpers.add(helper);
            int helperID = helpers.size()-1;

            try {
                System.out.println("Wann fängt der Helfer an zu arbeiten? Bitte im Format HH:mm angeben?");
                startTime = br.readLine();
                System.out.println("Wann hört der Helfer auf zu arbeiten? Bitte im Format HH:mm angeben?");
                endTime = br.readLine();
                System.out.println("Wieviel verdient der Helfer pro Stunde? z.B. 15,43");
                String s = br.readLine();
                //ToDo save with double zeros
                moneyPerHour = Double.parseDouble(s);
            } catch (IOException e) {
                e.printStackTrace();
            }

            Entry entry = new Entry(helperID,profileID,moneyPerHour,startTime,endTime);
            entries.add(entry);
            System.out.println("Eintrag wurde angelegt");

        }
    }
}
