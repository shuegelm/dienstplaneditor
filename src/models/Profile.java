package models;

public class Profile {

    private String profileName;
    private String skill;

    public Profile(String profileName, String skill) {
        this.profileName = profileName;
        this.skill = skill;
    }

    @Override
    public String toString() {
        return this.profileName + " mit der Anforderung: " + this.skill;
    }

    public String getProfileName() {
        return profileName;
    }

    public void setProfileName(String profileName) {
        this.profileName = profileName;
    }

    public String getSkill() {
        return skill;
    }

    public void setSkill(String skill) {
        this.skill = skill;
    }
}
