package models;

public class Helper {
    private String name;
    private String surname;
    private String iban;
    private String helperSkill;

    public Helper(String name, String surname, String iban, String helperSkill) {
        this.name = name;
        this.surname = surname;
        this.iban = iban;
        this.helperSkill = helperSkill;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getIban() {
        return iban;
    }

    public void setIban(String iban) {
        this.iban = iban;
    }

    public String getHelperSkill() {
        return helperSkill;
    }

    public void setHelperSkill(String helperSkill) {
        this.helperSkill = helperSkill;
    }
}
