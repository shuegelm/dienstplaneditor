package models;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Entry {

    private int helperID;
    private int profileID;
    private double moneyPerHour;
    private String startTime;
    private String endTime;

    public Entry(int helperID, int profileID, double moneyPerHour, String startTime, String endTime) {
        this.helperID = helperID;
        this.profileID = profileID;
        this.moneyPerHour = moneyPerHour;
        this.startTime = startTime;
        this.endTime = endTime;
    }

    public long calculateTime() {
        SimpleDateFormat format = new SimpleDateFormat("HH:mm");
        Date date1 = null;
        Date date2 = null;
        try {
            date1 = format.parse(this.startTime);
            date2 = format.parse(this.endTime);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        //time in miliseconds, /1000 = seconds, /3600 = hours
        return (date2.getTime() - date1.getTime())/3600000;
    }

    public double calculateEarning() {
        return this.moneyPerHour * this.calculateTime();
    }

    @Override
    public String toString() {
        return " arbeitet von " + this.startTime + " bis "+ this.endTime + " für " + this.moneyPerHour + " Euro die Stunde.";
    }

    public int getHelperID() {
        return helperID;
    }

    public void setHelperID(int helperID) {
        this.helperID = helperID;
    }

    public int getProfileID() {
        return profileID;
    }

    public void setProfileID(int profileID) {
        this.profileID = profileID;
    }

    public double getMoneyPerHour() {
        return moneyPerHour;
    }

    public void setMoneyPerHour(double moneyPerHour) {
        this.moneyPerHour = moneyPerHour;
    }

    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public String getEndTime() {
        return endTime;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }
}
